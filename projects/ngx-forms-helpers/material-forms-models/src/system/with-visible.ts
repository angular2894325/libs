import {
    Signal,
} from "@angular/core";

export namespace WithVisible {
    export interface Implement {
        readonly isVisible: Signal<boolean>
    }

    export interface Params {
        isVisible?: boolean
    }
}
