import {
      signal,
      WritableSignal
} from "@angular/core";

import {
    MatFormFieldBaseModel,
    MatFormFieldBaseModelParams
} from "./base/mat-form-field-base.model";

export interface Option<T = unknown> {
      value: T
      label: string
      disabled?: boolean
}

export type SelectSource<T = unknown> = Option<T>[]

type Params = Partial<Record<
    | 'label'
    ,
    string
>>

export interface FormSelectModelParams<Value = unknown>
    extends MatFormFieldBaseModelParams<Value>,
        Params
{
    disableRipple?: boolean
    source?: SelectSource<Value>
    options?: SelectSource<Value>
}

export class FormSelectModel<Value = unknown>
    extends MatFormFieldBaseModel<Value>
{
    public readonly source: WritableSignal<SelectSource>;
    public readonly options: WritableSignal<SelectSource>;
    public readonly disableRipple: WritableSignal<boolean>;

    constructor(params: FormSelectModelParams<Value>) {
        super(params);

        const {
            source,
            options,
            disableRipple,
        } = params;

        this.source = signal(source ?? []);
        this.options = signal(options ?? []);
        this.disableRipple = signal(disableRipple ?? false);
    }
}
