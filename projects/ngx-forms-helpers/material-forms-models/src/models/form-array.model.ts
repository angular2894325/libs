import {
    signal,
    WritableSignal
} from "@angular/core";

import {WithVisible} from "../system/with-visible";
import {BaseFormArrayModel, BaseFormArrayModelParams, BaseFormModels } from "ngx-forms-helpers/base-forms-models";

interface FormArrayModelParams<Models extends BaseFormModels[] = any>
    extends BaseFormArrayModelParams<Models>,
        WithVisible.Params
{}

export class FormArrayModel<Models extends BaseFormModels[] = any>
    extends BaseFormArrayModel<Models>
    implements WithVisible.Implement
{
    public readonly isVisible: WritableSignal<boolean>;
    constructor(params: FormArrayModelParams<Models>) {
        super(params);

        const {
            isVisible,
        } = params;

        this.isVisible = signal(isVisible ?? true);
    }
}
