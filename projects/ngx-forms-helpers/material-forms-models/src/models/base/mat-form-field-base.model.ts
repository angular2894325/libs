import {
    signal,
    WritableSignal
} from "@angular/core";
import {BaseFormControlModel, BaseFormControlModelParams } from "ngx-forms-helpers/base-forms-models";

type Params = Partial<Record<
    | 'label'
    | 'suffixIcon'
    | 'prefix'
    | 'suffix'
    | 'hint'
    ,
    string
>>

export interface MatFormFieldBaseModelParams<Value = unknown>
    extends
        BaseFormControlModelParams<Value>,
        Params
{
    floatLabel?: 'always' | 'auto'
}

export class MatFormFieldBaseModel<Value = unknown> extends BaseFormControlModel<Value> {
    public readonly floatLabel: WritableSignal<'always' | 'auto'>;
    public readonly label: WritableSignal<string>;
    public readonly suffixIcon: WritableSignal<string>;
    public readonly prefix: WritableSignal<string>;
    public readonly suffix: WritableSignal<string>;
    public readonly hint: WritableSignal<string>;

    constructor(params: MatFormFieldBaseModelParams<Value>) {
        super(params);

        const {
            label,
            suffix,
            prefix,
            suffixIcon,
            floatLabel,
            hint,
        } = params;

        this.floatLabel = signal(floatLabel ?? 'auto');
        this.label = signal(label ?? '');
        this.suffixIcon = signal(suffixIcon ?? '');
        this.prefix = signal(prefix ?? '');
        this.suffix = signal(suffix ?? '');
        this.hint = signal(hint ?? '');
    }
}
