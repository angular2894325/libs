import {
    signal,
    Signal
} from "@angular/core";

import {WithVisible} from "../system/with-visible";
import {BaseFormGroupModel, BaseFormGroupModelParams, ModelsRecord } from "ngx-forms-helpers/base-forms-models";

export interface FormGroupModelParams<Models extends ModelsRecord = any>
    extends BaseFormGroupModelParams<Models>,
        WithVisible.Params
{}

export class FormGroupModel<Models extends ModelsRecord = any>
    extends BaseFormGroupModel<Models>
    implements WithVisible.Implement
{
    public readonly isVisible: Signal<boolean>;

    constructor(params: FormGroupModelParams<Models>) {
        super(params);

        const {
            isVisible
        } = params;

        this.isVisible = signal(isVisible ?? true);
    }
}
