import {
      signal,
      WritableSignal
} from "@angular/core";

import {WithVisible} from "../system/with-visible";
import {
    MatFormFieldBaseModel,
    MatFormFieldBaseModelParams
} from "./base/mat-form-field-base.model";

export type InputTypes =
    | 'color'
    | 'date'
    | 'datetime-local'
    | 'email'
    | 'month'
    | 'number'
    | 'password'
    | 'search'
    | 'tel'
    | 'text'
    | 'time'
    | 'url'
    | 'week'

type Params = Partial<Record<
    | 'label'
    | 'placeholder'
    | 'suffixIcon'
    | 'prefix'
    | 'suffix'
    | 'hint'
    ,
    string
>>

export interface FormInputModelParams<Value = unknown>
    extends MatFormFieldBaseModelParams<Value>,
        WithVisible.Params,
        Params
{
    inputType?: InputTypes
}

export class FormInputModel<Value = unknown>
    extends MatFormFieldBaseModel<Value>
{
    public readonly inputType: WritableSignal<InputTypes>;
    public readonly placeholder: WritableSignal<string>;

    constructor(params: FormInputModelParams<Value>) {
        super(params);

        const {
            inputType,
            placeholder,
        } = params;

        this.placeholder = signal(placeholder ?? '');
        this.inputType = signal(inputType ?? 'text');
    }
}
