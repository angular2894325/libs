import {
    AbstractControl,
    FormControlState,
} from "@angular/forms";

import {
    Observable,
    startWith,
} from "rxjs";

type ExcludeFormControlState<T> = T extends FormControlState<any> ? never : T;

export function withStartValue<T>(control: AbstractControl<ExcludeFormControlState<T>>): Observable<ExcludeFormControlState<T>> {
    return control.valueChanges
        .pipe(
            startWith(control.value)
        );
}