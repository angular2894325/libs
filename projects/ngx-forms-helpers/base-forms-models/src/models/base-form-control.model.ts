import {
    FormControl,
    FormControlOptions,
} from '@angular/forms';

import {AbstractFormModel} from './abstract-form-model';

export interface BaseFormControlModelParams<Value = unknown> {
    value: Value
    disabled?: boolean
    controlOptions?: FormControlOptions
}

export class BaseFormControlModel<Value = unknown> extends AbstractFormModel<Value> {
    public readonly control: FormControl<Value>;

    constructor({
        value,
        disabled,
        controlOptions,
    }: BaseFormControlModelParams<Value>) {
        super();

        this.control = new FormControl(value, {...controlOptions, nonNullable: true});
        this.handleInitialDisable(disabled);
    }
}
