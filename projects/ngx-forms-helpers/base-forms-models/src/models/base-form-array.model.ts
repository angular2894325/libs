import {
    AbstractControlOptions,
    FormArray,
} from '@angular/forms';

import {AbstractFormModel} from "./abstract-form-model";
import {BaseFormModels} from "../types/types";

export interface BaseFormArrayModelParams<Models extends BaseFormModels[] = any> {
    models: Models
    disabled?: boolean
    controlOptions?: AbstractControlOptions
}

export class BaseFormArrayModel<Models extends BaseFormModels[] = any> extends AbstractFormModel {
    public readonly control: FormArray<Models[number]['control']>;
    public readonly models: Models;

    constructor({
        models,
        disabled,
        controlOptions,
    }: BaseFormArrayModelParams<Models>) {
        super();

        this.models = models;
        this.control = new FormArray(this.models.map(item => item.control), controlOptions);
        this.handleInitialDisable(disabled);
    }

    public push(model: Models[number], emitEvent?: boolean): void {
        this.models.push(model);
        this.control.push(model.control, {emitEvent});
    }

    public removeAt(index: number, emitEvent?: boolean): void {
        this.models.splice(index, 1);
        this.control.removeAt(index, {emitEvent});
    }

    public setControl(index: number, model: Models[number], emitEvent?: boolean): void {
        this.models.splice(index, 1, model);
        this.control.setControl(index, model.control, {emitEvent});
    }

    public insert(index: number, model: Models[number], emitEvent?: boolean): void {
        this.models.splice(index, 0, model);
        this.control.insert(index, model.control, {emitEvent});
    }
}
