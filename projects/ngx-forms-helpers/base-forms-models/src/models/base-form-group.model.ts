import {
    AbstractControlOptions,
    FormGroup,
} from '@angular/forms';

import {
  BaseFormModels,
  Indexing
} from "../types/types";
import {AbstractFormModel} from "./abstract-form-model";

export type ModelsRecord<Models extends BaseFormModels = any> = Indexing<Models>;

export type FormGroupControls<Controls extends ModelsRecord = any> = {
    [key in keyof Controls]: Controls[key]['control']
};

export type FormGroupControl<Controls extends ModelsRecord = any> = FormGroup<FormGroupControls<Controls>>;

export interface BaseFormGroupModelParams<Models extends ModelsRecord = any> {
    models: Models
    disabled?: boolean
    controlOptions?: AbstractControlOptions
}

export class BaseFormGroupModel<Models extends ModelsRecord = any> extends AbstractFormModel {
    public readonly control: FormGroupControl<Models>;
    public readonly models: {[key in keyof Models]: Models[key]};

    constructor({
        models,
        disabled,
        controlOptions,
    }: BaseFormGroupModelParams<Models>) {
        super();

        this.models = models;
        this.control = new FormGroup(this.initializeControls(), controlOptions);
        this.handleInitialDisable(disabled);
    }

    public setControl<K extends string & keyof Models>(name: K, model: Models[K], emitEvent?: boolean): void {
        this.models[name] = model;
        this.control.setControl(name, model.control, {emitEvent});
    }

    public removeControl(name: keyof Models, emitEvent?: boolean): void {
        delete this.models[name];
        this.control.removeControl(name as any, {emitEvent});
    }

    public addControl<K extends string & keyof Models>(name: K, model: Models[K], emitEvent?: boolean): void {
        this.registerModel(name, model);
        this.control.addControl(name, model.control, {emitEvent});
    }

    public registerControl<K extends string & keyof Models>(name: K, model: Models[K]): Models[K] {
        const registeredModel = this.registerModel(name, model);
        this.control.registerControl(name, model.control);
        return registeredModel;
    }

    private registerModel<K extends string & keyof Models>(name: K, model: Models[K]): Models[K] {
        if (name in this.models) {
            return this.models[name];
        }

        return this.models[name] = model;
    }

    private initializeControls(): FormGroupControls<Models> {
        const entries: [keyof Models, Models[keyof Models]][] = Object
            .keys(this.models)
            .map((item: keyof Models) => [item, this.models[item]!.control]);

        return Object.fromEntries(entries) as FormGroupControls<Models>;
    }
}
