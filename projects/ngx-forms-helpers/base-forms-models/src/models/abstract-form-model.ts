import {AbstractControl} from '@angular/forms';

export abstract class AbstractFormModel<Value = unknown> {
    public abstract readonly control: AbstractControl<Value>;

    protected handleInitialDisable(disabled: boolean = false): void {
        if (disabled) this.control.disable();
    }
}
