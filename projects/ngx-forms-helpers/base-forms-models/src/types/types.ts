import {BaseFormGroupModel} from "../models/base-form-group.model";
import {BaseFormArrayModel} from "../models/base-form-array.model";
import {BaseFormControlModel} from "../models/base-form-control.model";

export type Indexing<T = unknown> = {
    [key: string]: T
}

export type BaseFormModels =
    | BaseFormGroupModel
    | BaseFormArrayModel
    | BaseFormControlModel
