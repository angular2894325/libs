import {
    ReactiveFormsModule,
    Validators
} from "@angular/forms";
import {
    Component,
    DestroyRef,
    inject,
    OnInit,
} from '@angular/core';
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";
import { RouterOutlet } from '@angular/router';

import {InputComponent} from "./forms/input/input.component";
import {withControlStartValue} from "./helpers/with-control-start-value";
import {SelectComponent} from "./forms/select/select.component";
import {SelectMultipleComponent} from "./forms/select-multiple/select-multiple.component";
import {
    FormGroupModel,
    FormInputModel,
    FormSelectModel
} from 'ngx-forms-helpers/material-forms-models'

@Component({
    selector: 'test-forms-root',
    standalone: true,
    imports: [
        RouterOutlet,
        InputComponent,
        ReactiveFormsModule,
        SelectComponent,
        SelectMultipleComponent,
    ],
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {
    private readonly destroyRef = inject(DestroyRef);

    protected readonly baseFormGroup = new FormGroupModel({
        models: {
            name: new FormInputModel({
                value: 'Dima',
                label: 'Name',
                disabled: true,
                controlOptions: {
                    validators: [Validators.required]
                }
            }),
            options: new FormSelectModel<number | undefined>({
                value: undefined,
                label: 'Options',
                options: [
                    {
                        value: undefined,
                        label: 'Not selected',
                    },
                    {
                        value: 1,
                        label: '1',
                    },
                    {
                        value: 2,
                        label: '2',
                    },
                    {
                        value: 3,
                        label: '3',
                        disabled: true,
                    },
                ],
            }),
            optionsMultiple: new FormSelectModel<number | undefined>({
                value: undefined,
                label: 'Options',
                options: [
                    {
                        value: 1,
                        label: '1',
                    },
                    {
                        value: 2,
                        label: '2',
                    },
                    {
                        value: 3,
                        label: '3',
                        disabled: true,
                    },
                ],
            }),
        },
    })

    public ngOnInit(): void {
        withControlStartValue(this.baseFormGroup.control)
            .pipe(takeUntilDestroyed(this.destroyRef))
            .subscribe(value => {
                console.log(value)
            });
    }
}
