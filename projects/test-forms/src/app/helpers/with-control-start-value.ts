import {AbstractControl} from "@angular/forms";
import {
  Observable,
  startWith,
} from "rxjs";

export function withControlStartValue<T>(control: AbstractControl<T>): Observable<T> {
  return control.valueChanges
    .pipe(
      startWith(control.value)
    )
}
