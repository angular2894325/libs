import {
    Component,
    input
} from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";
import {MatIcon} from "@angular/material/icon";
import {
    MatError,
    MatFormField,
    MatHint,
    MatLabel,
    MatPrefix,
    MatSuffix,
} from "@angular/material/form-field";
import {
    MatOption,
    MatSelect
} from "@angular/material/select";

import {AbstractValueAccessor} from "../../classes/abstract-value-accessor";
import {FormSelectModel} from 'ngx-forms-helpers/material-forms-models';

@Component({
    selector: 'test-forms-select-multiple',
    standalone: true,
    imports: [
        MatFormField,
        MatLabel,
        MatSelect,
        MatOption,
        ReactiveFormsModule,
        MatError,
        MatHint,
        MatIcon,
        MatPrefix,
        MatSuffix
    ],
    templateUrl: './select-multiple.component.html',
    styleUrl: './select-multiple.component.scss'
})
export class SelectMultipleComponent extends AbstractValueAccessor {
    public model = input.required<FormSelectModel>();
}
