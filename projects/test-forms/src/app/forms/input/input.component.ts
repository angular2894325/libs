import {
  Component,
  input,
} from '@angular/core';
import {
  MatError,
  MatFormField,
  MatHint,
  MatLabel,
  MatPrefix,
  MatSuffix,
} from "@angular/material/form-field";
import {MatInput} from "@angular/material/input";
import {
  ReactiveFormsModule,
} from "@angular/forms";
import {MatIcon} from "@angular/material/icon";

import {AbstractValueAccessor} from "../../classes/abstract-value-accessor";
import {FormInputModel} from 'ngx-forms-helpers/material-forms-models';

@Component({
    selector: 'test-forms-input',
    standalone: true,
    imports: [
        MatFormField,
        MatInput,
        MatLabel,
        ReactiveFormsModule,
        MatIcon,
        MatSuffix,
        MatPrefix,
        MatError,
        MatHint,
    ],
    templateUrl: './input.component.html',
    styleUrl: './input.component.scss'
})
export class InputComponent<Value = unknown> extends AbstractValueAccessor<Value> {
    public model = input.required<FormInputModel<Value>>();
}
