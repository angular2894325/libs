import {
  ControlValueAccessor,
  FormControl,
  NgControl,
} from "@angular/forms";
import {inject} from "@angular/core";

export class AbstractValueAccessor<Value = unknown> implements ControlValueAccessor {
  protected readonly ngControl = inject(NgControl);

  constructor() {
    this.ngControl.valueAccessor = this;
  }

  public get showError(): boolean {
    return this.control.invalid && this.control.touched;
  }

  public get control(): FormControl<Value> {
    return this.ngControl.control as FormControl;
  }

  public writeValue(value: unknown): void {
  }

  public registerOnChange(fn: Function): void {
  }

  public registerOnTouched(fn: Function): void {
  }

  public setDisabledState(isDisabled: boolean): void {
  }
}
