import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';
import { FormGroupModel, FormInputModel } from 'ngx-forms-helpers/material-forms-models';
import { withStartValue } from 'ngx-forms-helpers/utils/with-start-value';

bootstrapApplication(AppComponent, appConfig)
  .catch((err) => console.error(err));


const gr = new FormGroupModel({
    models: {
        name: new FormInputModel({value: 'test'})
    }
})

withStartValue(gr.control.controls.name)
    .subscribe(value => {
        value.repeat(1)
    })